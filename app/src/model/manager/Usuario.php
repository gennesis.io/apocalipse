<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Project\Model\Manager
 | @file: Usuario.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 24/04/16 20:07
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Project\Model\Manager;


use Apocalipse\Core\Domain\Auth\Auth;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Domain\Definition\Collection;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Model\Dynamo;

/**
 * Class Usuario
 * @package Apocalipse\Project\Model\Manager
 */
class Usuario extends Dynamo
{
    /**
     * Usuario constructor.
     */
    public function __construct()
    {
        parent::__construct(module(self::class),  entity(self::class), new Collection(environment('auth.collection')), 'json');

        $this->skeleton();
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    public function before($action, Record $record)
    {
        $usr_password = $record->get('usr_password');

        if ($usr_password) {
            $record->set('usr_password', Auth::crypt($usr_password));
        } else {
            $record->setPrivate('usr_password');
        }

        return parent::before($action, $record);
    }
}