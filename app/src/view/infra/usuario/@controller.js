/**
 * {{refactor.viewModel}}
 */
App.register("UsuarioController", ['$scope', 'ServiceListener', 'ServiceModel',
    function ($scope, ServiceListener, ServiceModel) {

        const id = $scope.module + '.' + $scope.entity + '.' + $scope.index;
        const __USR_ROLE__ = [
            {
                "id": "admin",
                "text": "Administrador"
            }
        ];

        ServiceListener.register('after.recover-record', id, function (record) {

            if (record) {
                record['usr_password'] = '';
            }
        });

        ServiceListener.register('after.edit', id, function () {

            const operation = ServiceModel.getOperation($scope.index);
            if (operation && operation.fields) {
                operation.fields.forEach(function (field) {
                    if (field.key === 'usr_role') {
                        field.options = __USR_ROLE__;
                    }
                });
            }
        });
    }]
);
