<?php

if (defined('__APP_ENV__')) {
    /** @noinspection PhpIncludeInspection */
    require path(__DIR__, __APP_ENV__, 'parameters.php');
}