<?php

/** @var \Apocalipse\Core\Domain\Content\Renderer $this */

?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title> <?php out(config('app.name')); ?> </title>
<meta name="theme-color" content="#63696f"/>
<link rel="shortcut icon" href="<?php asset('@/images/favicon.png'); ?>" type="image/x-icon">

<!-- Google Fonts -->
<!--https://design.google.com/icons/-->
<link href="<?php asset('google-fonts/material-icons/index.css'); ?>" rel="stylesheet">
<link href="<?php asset('google-fonts/play/index.css'); ?>" rel="stylesheet">

<!-- Bootstrap -->
<link href="<?php asset('bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php asset('bootstrap/css/sticky-footer-fixed.css'); ?>" rel="stylesheet">
<link href="<?php asset('bootstrap/css/index.css'); ?>" rel="stylesheet">

<!-- Bootstrap Plugins -->
<!--<link href="<?php asset('bootstrap/plugins/bootstrap-dialog/css/bootstrap-dialog.min.css'); ?>" rel="stylesheet">-->

<!-- Bootstrap Theme -->
<?php

$this->append('themes/paper');

?>

<!-- Font Awesome -->
<link href="<?php asset('font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">

<!-- JQuery -->
<script src="<?php asset('jquery/jquery.min.js'); ?>"></script>
<script src="<?php asset('jquery/plugins/jquery.mousewheel.min.js'); ?>"></script>
<script src="<?php asset('jquery/plugins/jquery.simplr.smoothscroll.js'); ?>"></script>
<script src="<?php asset('jquery/plugins/jquery.browser.js'); ?>"></script>

<!-- Bootstrap -->
<script src="<?php asset('bootstrap/js/bootstrap.min.js'); ?>"></script>

<!-- Bootstrap Plugins -->
<script src="<?php asset('bootstrap/plugins/bootstrap-dialog/js/bootstrap-dialog.min.js'); ?>"></script>


<link href="<?php asset('@/css/index.css'); ?>" rel="stylesheet">

