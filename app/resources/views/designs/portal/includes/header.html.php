<div class="header-panel brand-mark">

    <div class="container">

        <div class="col-xs-12 hidden visible-xs">
            <img src="<?php url('static/app/img/logo.png'); ?>" alt="logo">
        </div>

        <div class="col-sm-10 col-md-8 hidden-xs">
            <a href="<?php url('index'); ?>">
                <h1 class="font-play"><?php out(config('app.name')); ?></h1>
            </a>
        </div>

        <div class="col-sm-2 col-md-4 hidden-sm hidden-xs">
            <form class="form-inline" action="<?php url('search/'); ?>" method="get">
                <div class="pull-right">
                    <div class="input-group input-group-lg">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </span>
                        <input type="text" class="form-control" name="for" placeholder="Pesquisar...">
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>