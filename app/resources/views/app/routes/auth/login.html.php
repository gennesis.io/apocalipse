<div class="container container--login">

    <div class="card z-depth-1">

        <div class="card-header" style="border-bottom: 1px solid #E0E0E0;background: #fafafa;">
            <div class="pull-left">
                <h2>Entrar</h2>
            </div>
            <div class="pull-right">
                <button class="btn btn-danger waves-effect" ng-click="go('auth/register')"> Ainda n&atilde;o tenho cadastro </button>
            </div>
            <br style="clear: both;">
        </div>

        <div class="card-body card-padding" ng-init="credential = {login: 'admin', password: '123456'}">

            <form role="form">

                <div class="form-group fg-line">
                    <label for="no-cache-1">Login</label>
                    <input type="text" class="form-control" ng-model="credential.login" id="no-cache-1" placeholder="Informe o seu usuário ou e-mail" autocomplete="off">
                </div>
                <div class="form-group fg-line">
                    <label for="no-cache-2">Senha</label>
                    <input type="password" class="form-control" ng-model="credential.password" id="no-cache-2" placeholder="Informe sua senha" autocomplete="off">
                </div>

                <div style="height: 70px;border-top: 1px solid #ddd;padding: 5px 20px 0 20px;margin: 0 -25px;">

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" ng-model="credential.remember">
                            <i class="input-helper"></i> Lembrar
                        </label>
                    </div>

                    <div class="pull-left">
                        <button type="submit" class="btn bgm-indigo waves-effect" ng-click="login(credential)"
                                style="padding: 6px 40px; font-size: 14px;">Entrar </button>
                    </div>

                    <div class="pull-left">
                        <div class="preloader pl-sm" ng-show="logging">
                            <svg class="pl-circular" viewBox="25 25 50 50">
                                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                            </svg>
                        </div>
                    </div>

                    <div class="pull-right">
                        <button class="btn btn-link" ng-click="go('auth/reset')"> Esqueci a minha senha</button>
                    </div>

                </div>

            </form>
        </div>
    </div>