<div class="container container--login">

    <div class="card z-depth-1">

        <div class="card-header" style="border-bottom: 1px solid #E0E0E0;background: #fafafa;">
            <div class="pull-left">
                <h2>Cadastrar</h2>
            </div>
            <div class="pull-right">
                <button class="btn btn-danger waves-effect" ng-click="go('auth/login')"> J&aacute; tenho uma senha</button>
            </div>
            <br style="clear: both;">
        </div>

        <div class="card-body card-padding">

            <form name="form" ng-init="credential = {}">

                <div class="form-group fg-line">
                    <label for="no-cache-2">Login</label>
                    <input type="text" required class="form-control" ng-model="credential.login" id="no-cache-2"
                           placeholder="Informe seu nome de usuário ou e-mail" autocomplete="off">
                </div>

                <div class="form-group">
                    <p>
                        Ser&aacute; enviado para o seu e-mail um link para a recupera&ccedil;&atilde;o de senha.
                    </p>
                    <p>
                        Se preferir receber uma nova senha diretamente no seu e-mail marque a op&ccedil;&atilde;o abaixo
                        <i>"Recupera&ccedil;&atilde;o Simplificada"</i>
                    </p>
                </div>

                <div style="height: 75px;border-top: 1px solid #ddd;padding: 5px 20px 0 20px;margin: 0 -25px;">

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" ng-model="credential.simplify">
                            <i class="input-helper"></i>
                            Recupera&ccedil;&atilde;o Simplificada
                        </label>
                    </div>

                    <div class="pull-left">
                        <button type="submit" class="btn bgm-indigo waves-effect" ng-click="reset(credential)"
                                ng-disabled="!(!form.$invalid)"
                                style="padding: 6px 40px; font-size: 14px;">Recuperar
                        </button>
                    </div>

                    <div class="pull-left">
                        <div class="preloader pl-sm" ng-show="logging">
                            <svg class="pl-circular" viewBox="25 25 50 50">
                                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                            </svg>
                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>