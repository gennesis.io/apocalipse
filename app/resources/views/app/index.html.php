<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9" data-ng-app="App" data-ng-controller="materialadminCtrl as mactrl"><![endif]-->
<![if IE 9 ]><html data-ng-app="App" data-ng-controller="materialadminCtrl as mactrl"><![endif]>

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title> <?php out(config('app.name')); ?> </title>
        <meta name="theme-color" content="#63696f"/>
        <link rel="shortcut icon" href="<?php asset('@/images/favicon.png'); ?>" type="image/x-icon">

        <!-- Vendor CSS -->
        <link href="<?php asset('app/vendors/animate.css/animate.min.css&v=' . __APP_VERSION__); ?>" rel="stylesheet">
        <link href="<?php asset('app/vendors/material-design-iconic-font/dist/css/material-design-iconic-font.min.css&v=' . __APP_VERSION__); ?>" rel="stylesheet">
        <link href="<?php asset('app/vendors/bootstrap-sweetalert/lib/sweet-alert.css&v=' . __APP_VERSION__); ?>" rel="stylesheet">
        <link href="<?php asset('app/vendors/angular-loading-bar/src/loading-bar.css&v=' . __APP_VERSION__); ?>" rel="stylesheet">
        <link href="<?php asset('app/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css&v=' . __APP_VERSION__); ?>" rel="stylesheet">
        <link href="<?php asset('app/vendors/fullcalendar/dist/fullcalendar.min.css&v=' . __APP_VERSION__); ?>" rel="stylesheet">
        <link href="<?php asset('app/vendors/chosen/chosen.min.css&v=' . __APP_VERSION__); ?>" rel="stylesheet">
        <link href="<?php asset('app/vendors/hint.css/hint.min.css&v=' . __APP_VERSION__); ?>" rel="stylesheet">
        <link href="<?php asset('google-fonts/play/index.css&v=' . __APP_VERSION__); ?>" rel="stylesheet">

        <!-- CSS -->
        <link href="<?php asset('app/css/app.min.1.css&v=' . __APP_VERSION__); ?>" rel="stylesheet" id="app-level">
        <link href="<?php asset('app/css/app.min.2.css&v=' . __APP_VERSION__); ?>" rel="stylesheet">
        <link href="<?php asset('app/css/custom.css&v=' . __APP_VERSION__); ?>" rel="stylesheet">
        <link href="<?php asset('@/css/custom.css&v=' . __APP_VERSION__); ?>" rel="stylesheet">
        <link href="<?php asset('@/css/color.css&v=' . __APP_VERSION__); ?>" rel="stylesheet">

    </head>

    <body data-ng-class="{ 'sw-toggled': mactrl.layoutType === '1', 'logged': logged}" ng-controller="AppController">

        <header id="header" class="no-printable" data-current-skin="indigo" data-ng-include src="'template/header.html'" data-ng-controller="headerCtrl as hctrl"></header>

        <section id="main" class="no-printable">

            <aside id="sidebar" data-ng-include src="'template/sidebar-left.html'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>

            <section id="content" class="page-view" ng-view></section>

        </section>

        <section id="printable"></section>

        <!-- Core -->
        <script src="<?php asset('app/vendors/jquery/dist/jquery.min.js&v=' . __APP_VERSION__); ?>"></script>

        <!-- Angular -->
        <script src="<?php asset('app/vendors/angular/angular.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/vendors/angular/angular-route.min.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/vendors/angular/angular-cookies.min.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/vendors/angular-animate/angular-animate.min.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/vendors/angular-resource/angular-resource.min.js&v=' . __APP_VERSION__); ?>"></script>

        <!-- Angular Modules -->
        <script src="<?php asset('app/vendors/angular-loading-bar/src/loading-bar.js&v=' . __APP_VERSION__); ?>"></script>

        <!-- Common Vendors -->
        <script src="<?php asset('app/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/vendors/bootstrap-sweetalert/lib/sweet-alert.min.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/vendors/Waves/dist/waves.min.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/vendors/bootstrap-growl/bootstrap-growl.min.js&v=' . __APP_VERSION__); ?>"></script>


        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
        <script src="<?php asset('app/vendors/jquery-placeholder/jquery.placeholder.min.js&v=' . __APP_VERSION__); ?>"></script>
        <![endif]-->

        <!-- Using below vendors in order to avoid misloading on resolve -->
        <script src="<?php asset('app/vendors/fileinput/fileinput.min.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/vendors/chosen/chosen.jquery.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/vendors/angular-chosen-localytics/chosen.js&v=' . __APP_VERSION__); ?>"></script>


        <!-- App level -->
        <script src="<?php asset('app/js/app.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('@/config/' . __APP_ENV__ . '/env.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/config.js&v=' . __APP_VERSION__); ?>"></script>

        <script src="<?php asset('app/js/services.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/factory/ServiceApi.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/factory/ServiceModel.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/factory/ServiceOperation.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/factory/ServiceLayout.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/factory/ServiceInput.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/factory/ServiceOutput.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/factory/ServiceDialog.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/factory/ServicePopup.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/factory/ServiceListener.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/factory/ServiceView.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/factory/ServicePagination.js&v=' . __APP_VERSION__); ?>"></script>

        <script src="<?php asset('app/js/controllers/AppController.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/controllers/LoginController.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/controllers/ViewController.js&v=' . __APP_VERSION__); ?>"></script>

        <!-- Template Modules -->
        <script src="<?php asset('app/js/modules/calendar.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/modules/components.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/modules/form.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/modules/template.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/modules/ui.js&v=' . __APP_VERSION__); ?>"></script>
        <script src="<?php asset('app/js/modules/ui-bootstrap.js&v=' . __APP_VERSION__); ?>"></script>
    </body>
</html>