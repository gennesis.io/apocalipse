<div class="sidebar-inner c-overflow">


    <ul class="main-menu">
        <li>
            <a href="#/home" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Home</a>
        </li>

        <li ng-class="{'sub-menu toggled': !_permission.href}" ng-repeat="_permission in permissions">
            <a ng-href="{{ _permission.href }}" toggle-submenu data-ng-click="_permission.href ? mactrl.sidebarStat($event) : void(0)"> <i
                    ng-class="_permission.icon"></i> {{ _permission.name }} </a>
            <ul style="display: block;">
                <li>
                    <a ng-href="{{ _item.href }}" data-ng-click="mactrl.sidebarStat($event)" ng-repeat="_item in _permission.items">
                        <i ng-class="_item.icon"></i> {{ _item.name }}
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
