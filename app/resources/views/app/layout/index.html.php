<!-- container -->
<div class="container-fluid">

    <!-- card -->
    <div class="card z-depth-1">

        <!-- top bar -->
        <div class="listview lv-bordered lv-lg">

            <div class="lv-header-alt clearfix">

                <h2 class="lvh-label">
                    {{ view.model.getProperties().name }}
                    <small class="hidden-xs"> {{ view.model.getProperties().description }}  </small>
                </h2>

                <div class="lvh-search" ng-show="view.page.search.visible">
                    <input type="text" placeholder="Digite para pesquisar..." class="lvhs-input"
                           ng-model="view.page.search.text" ng-change="view.recover(index, view.page.settings.current)">

                    <i class="lvh-search-close" ng-click="view.page.search.hide()">&times;</i>
                </div>

                <ul class="lv-actions actions">
                    <li ng-repeat="_action in view.model.getActions(index, 'top')">
                        <a href="javascript:void(0);" ng-click="view.operation(_action.id)">
                            <i ng-class="_action.classIcon"></i>
                        </a>
                    </li>
                    <li style="display: none;">
                        <a href="" ng-click="view.page.search.show()"><i class="zmdi zmdi-search"></i></a>
                    </li>
                </ul>

                <ul class="pagination">
                    <li ng-class="{disabled: view.page.first()}">
                        <a href="javascript:void(0);" ng-click="view.recover(index, 1)">&laquo;</a>
                    </li>
                    <li ng-class="{disabled: view.page.previous()}">
                        <a href="javascript:void(0);" ng-click="view.recover(index, view.page.settings.current -1 )">&lsaquo;</a>
                    </li>
                    <li>
                        <input type="number" class="input" ng-model="view.page.settings.current" min="1"
                               ng-change="view.recover(index, view.page.settings.current)"/>
                    </li>
                    <li>
                        <div class="input--pages"> / {{ view.page.pages }}</div>
                    </li>
                    <li ng-class="{disabled: view.page.next()}">
                        <a href="javascript:void(0);" ng-click="view.recover(index, view.page.settings.current + 1)">&rsaquo;</a>
                    </li>
                    <li ng-class="{disabled: view.page.last()}">
                        <a href="javascript:void(0);" ng-click="view.recover(index, view.page.pages)">&raquo;</a>
                    </li>
                </ul>

                <div class="searchable">

                    <input type="text" class="input input--search" placeholder="Digite para pesquisar"
                           ng-model="view.page.search.text" ng-change="view.recover(index, view.page.settings.current)"
                           style="{{ (view.page.search.text ? 'background-color: rgb(255, 234, 232); border: 1px solid rgb(234, 180, 158);' : '') }}">

                    <input type="number" style="width: 70px;" class="input" ng-model="view.page.settings.skip" min="1"
                           ng-change="view.recover(index, view.page.settings.current)" title=""> <span> / {{ view.page.total }}</span>
                </div>

            </div>

        </div>

        <!-- data list -->
        <div class="listview lv-bordered lv-lg face list">

            <div class="lv-body">

                <div class="card--form no-records" ng-hide="view.model.recordSet.data.length">
                    No records to show
                </div>

                <div class="card--form table-responsive" ng-show="view.model.recordSet.data.length">

                    <table class="table table-condensed table-hover table-bordered">
                        <thead>
                        <tr>
                            <th width="150px"> # </th>
                            <th class="hidden visible-xs"> ~ </th>
                            <th class="hidden-xs {{:: _field.wrapper }}" ng-repeat="_field in view.model.getOperation(index).fields | orderBy:'order'">
                                {{::_field.label }}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="_record in view.model.recordSet.data">
                            <td>
                                <div class="table-options">
                                    <span ng-repeat="_action in view.model.getActions(index, 'middle')"
                                          data-hint="{{:: _action.hint }}" class="{{:: _action.hintPosition }}"
                                          style="margin: 0 2px;">
                                        <button title="" type="button" class="btn {{:: _action.className }}"
                                                ng-click="view.operation(_action.id, _record, _action.after)">
                                            <i class="{{:: _action.classIcon }}"></i>
                                        </button>
                                    </span>
                                </div>
                            </td>
                            <td class="hidden visible-xs">
                                <div class="table-resume">
                                    {{:: view.out.resume(view.model.getOperation(index).fields, _record) }}
                                </div>
                            </td>
                            <td class="hidden-xs {{:: _field.wrapper }}" ng-repeat="_field in view.model.getOperation(index).fields">
                                {{:: view.out.print( (_field.data ? _record[_field.data] : _record[_field.key]), _field.component ) }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>

        </div>

    </div>

</div>

<div class="fab-button--toolbar">

    <span
        ng-repeat="_action in view.model.getActions(index, 'fab')"
        data-hint="{{::_action.hint}}" ng-class="_action.hintPosition" style="margin: 0 2px;">

        <button type="button" class="btn btn-float"
                ng-class="_action.className"
                ng-click="view.operation(_action.id, view.model.record, _action.after)"
                ng-disabled="view.model.getOperation(_action.id).type === 'action' ? form.$invalid : false">
            <i class="{{:: _action.classIcon }}"></i>
        </button>
    </span>
</div>
