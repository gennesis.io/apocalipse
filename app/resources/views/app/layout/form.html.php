<!-- container -->
<div class="container-fluid">

    <!-- card -->
    <div class="card z-depth-1">

        <div class="listview lv-bordered lv-lg">

            <div class="lv-header-alt clearfix">

                <h2 class="lvh-label"> {{:: view.model.getProperties().name }} </h2>
                <small class="hidden-xs"> {{:: view.model.getProperties().description }} {{:: view.model.record[__id__] }}</small>

                <ul class="lv-actions actions">
                    <li ng-repeat="_action in view.model.getActions(index, 'top')">
                        <a href="javascript:void(0);" ng-click="view.operation(_action.id)"
                           data-hint="{{::_action.hint}}" ng-class="_action.hintPosition">
                            <i ng-class="_action.classIcon"></i>
                        </a>
                    </li>
                </ul>
            </div>

        </div>

        <div class="card--form">

            <form name="form">

                <div name-controller="name + 'Controller'">

                    <div class="col-sm-12">
                        <div class="form-group">
                            <span ng-repeat="_action in view.model.getActions(index, 'top')"
                                  data-hint="{{::_action.hint}}" ng-class="_action.hintPosition" style="margin: 0 2px;">

                                <button type="button" class="btn" ng-class="_action.className"
                                        ng-hide="_action.hide"
                                        ng-click="view.operation(_action.id, view.model.record, _action.after)"
                                        ng-disabled="view.model.getOperation(_action.id).type === 'action' ? form.$invalid : false">
                                    <i class="{{:: _action.classIcon }}"></i>
                                    {{:: _action.label }}
                                </button>
                            </span>
                        </div>
                        <hr>
                    </div>

                    <div ng-repeat="_field in view.model.getOperation(index).fields | orderBy:'order'">
                        <form-field form="form" field="_field"
                                    items="view.model.getOperation(index).fields"
                                    record="view.model.record"
                                    listener="listener"></form-field>
                    </div>

                    <div class="col-sm-12">
                        <hr>
                        <div class="form-group">
                            <span ng-repeat="_action in view.model.getActions(index, 'bottom')"
                                  data-hint="{{::_action.hint}}" ng-class="_action.hintPosition" style="margin: 0 2px;">

                                <button type="button" class="btn" ng-class="_action.className"
                                        ng-hide="_action.hide"
                                        ng-click="view.operation(_action.id, view.model.record, _action.after)"
                                        ng-disabled="view.model.getOperation(_action.id).type === 'action' ? form.$invalid : false">
                                    <i class="{{:: _action.classIcon }}"></i>
                                    {{:: _action.label }}
                                </button>
                            </span>
                        </div>
                    </div>

                </div>

            </form>

        </div>

        <br style="clear: both;">
    </div>

    {{ (debug ? view.model.record : '') }}
</div>

<div class="fab-button--toolbar">

    <span
        ng-repeat="_action in view.model.getActions(index, 'fab')"
        data-hint="{{::_action.hint}}" ng-class="_action.hintPosition" style="margin: 0 2px;">

        <button type="button" class="btn btn-float"
                ng-class="_action.className" ng-hide="_action.hide"
                ng-click="view.operation(_action.id, view.model.record, _action.after)"
                ng-disabled="view.model.getOperation(_action.id).type === 'action' ? form.$invalid : false">
            <i class="{{:: _action.classIcon }}"></i>
        </button>
    </span>
</div>
