
App._url = '..';
App.api = App._url  + '/api/head';
App.upload = App._url  + 'static/upload';
App.download = App._url  + 'static/download';
App.header = 'X-Auth-Token';
App.cookies = {
    path: '/'
};
App.ref = '_csrf';
App.token = '';
App.id = '_id';
App.exit = '';
App.code = true;
App.debug = false;