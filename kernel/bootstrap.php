<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @file: bootstrap.php
 -------------------------------------------------------------------
 | @user: william
 | @creation: 19/03/16 07:54
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP file to require basic resources of kernel's app
 |
 */


/** @noinspection PhpIncludeInspection */
require_once __APP_DIR_ROOT__ . '/kernel/helper/function.php';

/** @noinspection PhpIncludeInspection */
require_once path(__APP_DIR_ROOT__, 'kernel', 'helper', 'environment.php');

/** @noinspection PhpIncludeInspection */
require_once path(__APP_DIR_ROOT__, 'kernel', 'helper', 'error.php');

/** @noinspection PhpIncludeInspection */
require_once path(__APP_DIR_ROOT__, 'kernel', 'helper', 'autoload.php');

/** @noinspection PhpIncludeInspection */
require_once path(__APP_DIR_ROOT__, 'kernel', 'helper', 'shutdown.php');


if (file_exists(path(true, 'config', 'parameters.php'))) {
    /** @noinspection PhpIncludeInspection */
    require_once path(true, 'config', 'parameters.php');
}
