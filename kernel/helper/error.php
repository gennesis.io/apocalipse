<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @file: error.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 19/03/16 07:52
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP file to capture errors in process
 |
 */

use Apocalipse\Core\Flow\Wrapper;

/**
 * @param $type
 * @param $message
 * @param $file
 * @param $line
 */
set_error_handler(function ($type, $message, $file, $line) {

    Wrapper::push($message, Wrapper::STATUS_ERROR, $file, $line);
});
