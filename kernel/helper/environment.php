<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @file: environment.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 19/03/16 08:05
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | Configure environment modifying ini settings default;
 | Please, consider change ini file to that specifications;
 |
 */

error_reporting(E_ALL);

ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
ini_set('track_errors', 'Off');
ini_set('html_errors', 'Off');


/**
 * "boolean"
 * "integer"
 * "double" (for historical reasons "double" is returned in case of a float, and not simply "float")
 * "string"
 * "array"
 * "object"
 * "resource"
 * "NULL"
 * "unknown type"
 */
define('TYPE_BOOLEAN', 'boolean');
define('TYPE_INTEGER', 'integer');
define('TYPE_DOUBLE', 'double');
define('TYPE_STRING', 'string');
define('TYPE_ARRAY', 'array');
define('TYPE_OBJECT', 'object');
define('TYPE_RESOURCE', 'resource');
define('TYPE_NULL', 'null');
define('TYPE_UNKNOWN_TYPE', 'unknown type');