<?php

if (!file_exists(__DIR__ . '/.dev')) {

    if (isset($_POST['done']) && $_POST['done'] === 'true') {
        file_put_contents(__FILE__, '');
        ?>
        <h1>Done</h1>
        <p>Click <a href="<?php url('index', true, true); ?>">here</a> to continue to demo screen</p>
        <?php
    } else {
        ?>
        <h1>Setup</h1>
        <form method="post" action="">
            Hello dev! Before you start at run the project you need to do some stuffs:
            <ul>
                <li>
                    Configure a ".env" file into "app/config" dir. By default we will use test like the ".env" loaded
                </li>
                <li>
                    Check "drivers.json" specs into "app/config/{env}"
                </li>
                <li>
                    Check dependencies with composer (or by yourself) if need
                </li>
            </ul>
            <input type="hidden" name="done" value="true"/>
            <input type="submit" value="Ok!"/>
        </form>
        <?php
    }

    exit;
}
