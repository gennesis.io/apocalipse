<?php

$current = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$requirements = ['curl' => false, 'exec' => false, 'user' => false, 'allow_override' => false, 'mod_rewrite' => false];

foreach ($requirements as $requirement => $status) {

    switch ($requirement) {

        case 'curl':
            $curl = function_exists('curl_version');
            $requirements[$requirement] = $curl;
            if (!$curl) {
                break;
            }
            break;

        case 'exec':
            $curl = function_exists('exec');
            $requirements[$requirement] = $curl;
            if (!$curl) {
                break;
            }
            break;

        case 'user':
            $requirements[$requirement] = exec('whoami') !== 'www-data';
            break;

        case 'allow_override':
            $url = $current . '/allow_override/index.php';
            $requirements[$requirement] = get_headers($url, 1)[0] === 'HTTP/1.0 200 OK';
            break;

        case 'mod_rewrite':
            $url = $current . '/mod_rewrite/index.php';
            $requirements[$requirement] = get_headers($url, 1)[0] === 'HTTP/1.0 200 OK';
            break;
    }
}

?>
<table>
    <?php
    foreach ($requirements as $requirement => $status) {
        ?>
        <tr class="<?php echo $status ? 'success' : 'error'; ?>">
            <td><?php echo $requirement; ?></td>
            <td><?php echo $status ? ':)' : ':('; ?></td>
        </tr>
        <?php
    }
    ?>
</table>
<style>
    table, td {
        border: 1px solid #d0d0d0;
        border-collapse: collapse;
        padding: 5px 10px;
    }
    tr.success {
        background-color: green;
        color: white;
    }
    tr.error {
        background-color: red;
    }
</style>