<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @file: index.php
 -------------------------------------------------------------------
 | @user: william
 | @creation: 05/03/16 07:54
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | Load the app core
 |
 */

define('__APP_START__', microtime());

define('__APP_DIR_ROOT__', dirname(__DIR__));
define('__APP_DIR__', implode(DIRECTORY_SEPARATOR, [__APP_DIR_ROOT__, 'app']));
define('__APP_ENV__', env(dirname(__DIR__)));

/** @noinspection PhpIncludeInspection */ /* @exit on OPTIONS method of CORS */
require_once implode(DIRECTORY_SEPARATOR, [__APP_DIR_ROOT__, 'kernel', 'cors.php']);

/** @noinspection PhpIncludeInspection */
require_once implode(DIRECTORY_SEPARATOR, [__APP_DIR_ROOT__, 'kernel', 'bootstrap.php']);


use Apocalipse\Core\App;

if (!isset($service)) {
    $service = App::input('app-service');
}

define('__APP_URL__', App::getUrlRoot($service));
define('__APP_URL_CURRENT__', __APP_URL__ . '/' . $service);
define('__APP_URL_SERVICE__', $service);
define('__APP_URL_QUERY__', App::getUrlQueryString());


/** @setup `install` & `setup` */
require 'install.php';

/**
 * create a Response
 */
$response = App::http($service);

/*
 * generate:
 *   - headers
 *   - body
 */
App::response($response);